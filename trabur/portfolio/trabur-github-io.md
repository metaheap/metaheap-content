---
slug: trabur-github-io
title: trabur.github.io
description: This turns a json file into a presentable resume and is programmed in AngularJS. Also available in PDF format. Just download the generated PDF or use the browsers built in print funcionality.
cover: /static/portfolio/trabur-github-io.png
coverPreview: /static/portfolio/trabur-github-io.png
type: web
tags: [
  'AngularJS',
  "HTML",
  "CSS",
  "Gulp",
]
---

hello web