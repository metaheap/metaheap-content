---
slug: registration-docbookmd-com
title: registration.docbookmd.com
description: Seven step registration process including physician directory search, contact info confirmation, dynamic knowledge-based authentication (KBA), signature agreement, password creation, and login.
duration: "2 Weeks"
highlights: [
  "Overhauled existing problematic registration system collaborating with stakeholders and in house designers throughout design comps and development.",
  "Seven step registration process including physician directory search, contact info confirmation, dynamic knowledge-based authentication (KBA), signature agreement, password creation, and login.",
  "https://registration.docbookmd.com"
]
cover: /static/portfolio/registration-docbookmd-com.png
coverPreview: /static/portfolio/registration-docbookmd-com.png
type: web
tags: [
  "MySQL / MariaDB",
  "Cross Browser",
  "Express",
  "Twitter Bootstrap",
  "Sass",
  "Lodash",
  "HTML",
  "CSS",
  "Gulp",
  "AngularJS",
  "JavaScript"
]
---

hello web