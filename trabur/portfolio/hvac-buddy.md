---
slug: hvac-buddy
title: HVAC Buddy®
description: Using react-native deploying natively to both android and iOS platforms under a single language is now possible. This version of the app mirrors all the same functionalty as before but with this new language.
cover: /static/portfolio/hvac-buddy.png
coverPreview: /static/portfolio/hvac-buddy.png
type: mobile
tags: [
  'React Native'
]
---

hello mobile