---
slug: www-fleetgrid-com-console
title: www.fleetgrid.com/console
description: Employees, office staff and crew members, need to connect with people who consume service, residential and commercial customers. The FLEETGRID Console is built for employees to do just that.
cover: /static/portfolio/web.png
coverPreview: /static/portfolio/web.png
type: web
tags: [
  'React Native', 
  'Next.js', 
  'CSS',
  'HTML',
  'JSON',
  'GraphQL'
]
---

hello mobile