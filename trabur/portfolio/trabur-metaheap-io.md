---
slug: trabur-metaheap-io
title: trabur.metaheap.io
description: My personal website containing a blog and portfolio. All content starts out as markdown files. The markdown files are then turned into json files using processmd.
cover: /static/portfolio/trabur-metaheap-io.png
coverPreview: /static/portfolio/trabur-metaheap-io.png
type: web
tags: [
  'React Native',
  'HTML',
  'CSS',
  'Next.js',
  'processmd',
  'JSON'
]
---

hello web