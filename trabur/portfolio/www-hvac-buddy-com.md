---
slug: www-hvac-buddy-com
title: www.hvac-buddy.com
description: The landing page and showcase of all the HVAC Buddy applications. Contains a contact form that integrates with Gitlabs's support ticket system.
cover: /static/portfolio/www-hvac-buddy-com.png
coverPreview: /static/portfolio/www-hvac-buddy-com.png
type: web
tags: [
  'HTML',
  'CSS',
  'Next.js'
]
---

hello web