---
slug: app-gritness-com
title: app.gritness.com
description: Gritness was created by athletes who were fed-up with the level of effort required to get people together for a workout.
startDate: Apr. 2013
endDate: Nov. 2013
highlights: [
  "Collaborated with existing overseas developers, management, and stakeholders. Establishing core values, product gamification, and a business model.",
  "Implemented user profiles, settings, groups, activities, and a dashboard, with a focus on real time, using AJAX conventions with modal drop downs, partials, form validation, infinite scrolling, and X-editable.",
  "Using the Stripe payments API developed a complete payment system with shopping cart, invoices, fees, discounts, bank transfers, and an admin control panel to manage it all.",
  "http://gritness.com"
]
cover: /static/portfolio/app-gritness-com.png
coverPreview: /static/portfolio/app-gritness-com.png
type: web
tags: [
  "MySQL / MariaDB",
  "SQL",
  "Yii",
  "Cross Browser",
  "jQuery",
  "Twitter Bootstrap",
  "HTML",
  "CSS",
  "PHP",
  "JavaScript"
]
---
hello gritness