---
slug: fleetgrid
title: FLEETGRID
description: Using react-native deploying natively to both android and iOS platforms under a single language is now possible. It mirrors the same functionality found on the web. Also just like the web version it uses fleetgrid's API for application state so all changes are synced across the board.
cover: /static/portfolio/mobile.png
coverPreview: /static/portfolio/mobile.png
type: mobile
tags: [
  'GraphQL',
  'React Native',
  'HTML',
  'CSS',
]
---

hello mobile