---
slug: www-halotracks-org
title: www.halotracks.org
description: Created 20k member community gaming website, halotracks.org. Producing the most popular Halo 3-4 gametype and roughly 5,000 maps with over 20 million downloads.
startDate: Mar. 2008
endDate: 2011
highlights: [
  "Created 20k member community gaming website, halotracks.org. Producing the most popular Halo3-4 gametype and roughly 5,000 maps with over 20 million downloads.",
  "Integrating with bungie.net's public API overhauled existing IPB Gallery plugin to allow direct downloads and statistics.",
  "Learned the basics of creating, hosting, and maintaining LAMP."
]
cover: /static/portfolio/www-halotracks-org.png
coverPreview: /static/portfolio/www-halotracks-org.png
type: web
tags: [
  "MySQL / MariaDB",
  "SQL",
  "SEO",
  "jQuery",
  "HTML",
  "CSS",
  "PHP",
  "JavaScript"
]
---

hello web