---
slug: www-fleetgrid-com-static-voyager
title: www.fleetgrid.com/static/voyager/index.html
description: Got a sick GraphQL API and want to better visualize it? Try Voyager! I installed it in my Next.js static folder and it integrates with the currently logged in user session.
cover: /static/portfolio/www-fleetgrid-com-static-voyager.png
coverPreview: /static/portfolio/www-fleetgrid-com-static-voyager.png
type: nodejs
tags: [
  'GraphQL', 
  'Next.js', 
  'GraphQL Voyager'
]
---

hello mobile