---
slug: postgresql-to-rabbitmq
title: PostgreSQL to RabbitMQ
description: When you need to run a script of node.js code after a change in the database what do you do? You build a microservice to LISTEN to SQL events and push them to RabbitMQ for later digestion.
cover: /static/portfolio/nodejs.png
coverPreview: /static/portfolio/nodejs.png
type: nodejs
tags: [
  'SQL',
  'RabbitMQ',
  'PostgresQL',
  'AMQP'
]
---

hello node