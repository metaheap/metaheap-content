---
slug: api-fleetgrid-com-graphql
title: api.fleetgrid.com/graphql
description: Instant high-performance GraphQL API for your PostgreSQL database. Fleetgrid's web and mobile UI use this backend for application state and it works flawlessly.
cover: /static/portfolio/api-fleetgrid-com-graphql.png
coverPreview: /static/portfolio/api-fleetgrid-com-graphql.png
type: nodejs
tags: [
  'GraphQL',
  'Express',
  'Kubernetes',
  'Helm',
  'PostgreSQL',
  'PostGraphile',
  'JSON'
]
---

hello mobile