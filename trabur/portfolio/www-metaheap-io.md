---
slug: www-metaheap-io
title: www.metaheap.io
description: Splash page of my local network. Useful for directing me to services that I have running around the house like Plex.
cover: /static/portfolio/www-metaheap-io.png
coverPreview: /static/portfolio/www-metaheap-io.png
type: web
tags: [
  'HTML',
  'CSS',
  'Next.js'
]
---

hello web