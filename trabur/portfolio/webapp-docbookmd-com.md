---
slug: webapp-docbookmd-com
title: webapp.docbookmd.com
description: DocbookMD's flagship product for the web. A one to one representation to their existing mobile applications. Great attention to detail to ensure the same experience functionally and visually from mobile to web and back.
startDate: Apr. 2014
endDate: Jul. 2014
highlights: [
  "DocbookMD's flagship product for the web. A one to one representation to their existing mobile applications. Great attention to detail to ensure the same experience functionally and visually from mobile to web and back.",
  "Consists of email like instant messaging application. Physician, Careteam, and pharmacy directories with master detail pages and complex server logic. Including admin statistics, configuration, and management of messages, physicians, Careteams, etc.",
  "Performance tuning with transitions, large sets of sorted, filtered, and paginated data, and infinite scrolling.",
  "Cross browser down to Windows XP / IE8 support and HIPAA compliance across all DocbookMD products.",
  "https://webapp.docbookmd.com"
]
cover: /static/portfolio/webapp-docbookmd-com.png
coverPreview: /static/portfolio/webapp-docbookmd-com.png
type: web
tags: [
  "MySQL / MariaDB",
  "Cross Browser",
  "Express",
  "jQuery",
  "Twitter Bootstrap",
  "Sass",
  "Lodash",
  "HTML",
  "CSS",
  "Gulp",
  "Git",
  "AngularJS",
  "JavaScript"
]
---

hello web