---
slug: oneway-docbookmd-com
title: oneway.docbookmd.com
description: One way bulk message composition and dispatching to a network of web and mobile devices via sockets, tasks, and push notifications. Implementing audit logs, progress indicators, and message tracking tools to ensure critical messages were sent in a timely and efficient manner.
startDate: Feb. 2014
endDate: Mar. 2014
highlights: [
  "One way bulk message composition and dispatching to a network of web and mobile devices via sockets, tasks, and push notifications. Implementing audit logs, progress indicators, and message tracking tools to ensure critical messages were sent in a timely and efficient manner.",
  "Inherited from a former employee completed and maintained project.",
  "https://oneway.docbookmd.com"
]
cover: /static/portfolio/oneway-docbookmd-com.png
coverPreview: /static/portfolio/oneway-docbookmd-com.png
type: web
tags: [
  "MySQL / MariaDB",
  "Cross Browser",
  "Express",
  "jQuery",
  "Twitter Bootstrap",
  "Sass",
  "HTML",
  "CSS",
  "Gulp",
  "Git",
  "AngularJS",
  "JavaScript"
]
---

hello web