---
slug: setting-up-drone-to-ship-code-fast
title: Setting Up Drone to Ship Code Fast
description: 
cover: /static/blog/ShippingCodeFast/ship-code-fast.png
coverPreview: /static/blog/ShippingCodeFast/ship-code-fast.png
tags: ['CLI', 'git', 'ci', 'cd']
createdAt: 2018-11-13T20:16:25.873Z
---
Setting Up Drone to Ship Code Fast
========

Part of [Spinning Up An Ubuntu Machine](/blog/spinning-up-an-ubuntu-machine) is a step dedicated to setting up drone.io. In this blog post I am going to go over the setup process and configuration that I have running. 