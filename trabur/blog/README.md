---
slug: readme
title: README.md
description: Learn about this (trabur.metaheap.io) project's commands and internal file/folder structure.
cover: /static/blog/README/readme.png
coverPreview: /static/blog/README/readme.png
tags: ['README', 'CLI', 'git', 'json']
createdAt: 2018-11-13T20:16:25.873Z
---
TRAVIS BURANDT
========
Starting from the CLI. Clone the repo into the project folder:
```
$ cd ~/Projects
$ git clone git@gitlab.com:metaheap/trabur.metaheap.io.git
$ cd trabur.metaheap.io
```

Here is what the file/folder structure looks like:
- `./blog` contains blog articles in md
- `./components` contains react components used by ./pages
- `./pages` contains website code in next.js
- `./portfolio` contains portfolio articles in md
- `./static` contains files served behind http://127.0.0.1:7548/static/
- `./static/output/blog` contains auto generated json from `./blog/*`
- `./static/output/portfolio` contains auto generated json from `./portfolio/*`
- `./utils` contails useful js code needed throughout the app

Make changes to any `.md` file and then execute:
```sh
$ npm run content
``` 

This will convert all of the content to `json` files which is the format this website uses to display information.

Let's take a look:
```sh
$ npm run dev
```

Wait for the signal that says...
```sh
> Ready on http://127.0.0.1:7548
```

Now, navigate to the browser address bar and jump to the following URL:
- [http://127.0.0.1:7548](http://127.0.0.1:7548)

You should see a clone of [trabur.metaheap.io](http://trabur.metaheap.io) being hosted from the machine.

Looking into the markdown of each blog post we can see that we have a header wrapped in three dashes (---) that looks like this:
```js
// ---
// slug: readme
// title: README.md
// description: learn about the project's commands and internal file/folder structure.
// cover: /static/blog/README/readme.png
// coverPreview: /static/blog/README/readme.png
// tags: ['README', 'CLI', 'git', 'json']
// createdAt: 2018-11-13T20:16:25.873Z
// ---
```

In order to generate the createdAt simply navigate the the browser's developer tools (option + command + i) and copy/paste the following into the console:
```js
var createdAt = new Date()
// undefined
createdAt.toISOString()
// "2018-11-13T20:16:25.873Z"
```

