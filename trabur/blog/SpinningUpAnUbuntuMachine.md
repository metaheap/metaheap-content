---
slug: spinning-up-an-ubuntu-machine
title: Spinning Up An Ubuntu Machine
description: Starting from a USB to configuring and running applications we'll setup this ubuntu machine for home use (plex), test/dev use (drone.io), and all the way to hosting/running/managing my personal website(s). 
cover: /static/blog/SpinningUpAnUbuntuMachine/ubuntu.png
coverPreview: /static/blog/SpinningUpAnUbuntuMachine/ubuntu.png
tags: ['ubuntu']
createdAt: 2018-11-13T20:16:25.873Z
---
Spinning Up An Ubuntu Machine
========

Part of [My Lab Setup](/blog/my-lab-setup-for-full-stack-javascript-development) is a machine running Ubuntu. For the sake of knowing what the heck I've done to this machine I am keeping a record of a reproduceable set of events. So if this thing ever crashes again, like it did yesterday, I will be able to bring it back online.

Let's begin at installation. Start out by burning the [latest Ubuntu ISO](https://www.ubuntu.com/download/desktop) onto any portable storage device such as a USB. 

<img alt="Image of Google Fiber Network Router/Switch" src="/static/blog/SpinningUpAnUbuntuMachine/usb-flash.png" style="width: calc(100% + 200px); left: -100px; position: relative;">

While the machine is turned off plug the stick in and then boot. Hold or press F12 when prompted to enter boot option. You should now have the option to select a device to boot from. Next, select the USB device plugged in and wait for the operating system to load.

Run through the installation as normal. I named the machine "bridgeputer" and gave it a strong password.

When installation completes, log in, and you should see the default ubuntu desktop. The first thing I did was take a screenshot, as depicted in cover photo, to show my blank unbuntu installation.

I then opened firefox to download chrome so I could then gain access to all of my chrome plugins including lastpass which I will need later to gain access to my credentials.

Next, I am going to configure drone on this machine:
- [Setting Up Drone to Ship Code Fast](/blog/setting-up-drone-to-ship-code-fast)

The above blog post should go over setting up drone.io as well as configuring it to deploy a load balancer to proxy traffic from subdomains to each of the running proccesses.


