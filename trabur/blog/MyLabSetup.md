---
slug: my-lab-setup-for-full-stack-javascript-development
title: My Lab Setup for Full Stack JavaScript Development
description: Let's jump into the ins and outs of developing software as a full stack javascript developer.
cover: /static/blog/MyLabSetup/lab-setup-stage-0.png
coverPreview: /static/blog/MyLabSetup/lab-setup-stage-0.png
tags: ['CLI', 'git', 'json', 'load balancer']
createdAt: 2018-11-13T20:16:25.873Z
---
My Lab Setup for Full Stack JavaScript Development
========
Specifically for building, testing, and running javascript programs. Everything begins with initializing a Git repository and putting it behind some sort of hardware. That could be locally on any computer (for building), within the same network (for testing), in the cloud (for production), or dedicated hardware in one's own datacenter(s). I am going to touch on each of these environments and step through the processes that I go through while programming as a Full Stack JavaScript Developer. I will be scaling my dev/testing capabilities in incremental stages starting out at stage 0 (just my laptop and the cloud) to stage 1 (load balancing, CI/CD, and an ARM cluster) and we’ll see where it goes from there. Most importantly I’ll be making software along the way so expect a few deep dives into some code as it pertains to my personal website, blog, and production running projects such as fleetgrid.

My workflow for programming fleetgrid or any similar application starts on my macbook pro by manipulating the hosts file to point [www.fleetgrid.development](http://www.fleetgrid.development), [api.fleetgrid.development](http://api.fleetgrid.development), etc to node.js processes running locally on the machine. This is nice because every time a change is made to my file system watchers immediately update things. However this is not a solution for showing other people outside the realm of my laptop so I will push to Git for now.

Once changes have been made locally and pushed to Gitlab a CI/CD pipeline will get triggered and before you know it the application is pushed to the cloud and running in production ([www.fleetgrid.com](http://www.fleetgrid.com)).

<img alt="Image of Google Fiber Network Router/Switch" src="/static/blog/MyLabSetup/fiber-logo.jpg" style="width: calc(100% + 200px); left: -100px; position: relative;">

There is also my home network setup. From google fiber the network is given a static IP of 136.62.76.184. In the configuration port 80 is being forwarded to a node.js load balancer. The LB handles traffic for [www.metaheap.io](http://www.metaheap.io), [plex.metaheap.io](http://plex.metaheap.io), and [trabur.metaheap.io](http://trabur.metaheap.io). Everything points to my laptop which makes this stage 0 of my setup; The most basic and simplest of them all. I could continue hosting everything from my laptop, but anything hosted on it is offline when the laptop is powered off. Not good.

More on [My Network Setup](/blog/my-network-setup) in another blog post.

Stage 0 being just my laptop for developing and testing software has been great and I have been able to achieve a lot with just 1 computer with a 13 inch screen. However it would be nice to accomplish more. Then there is this Dell i5 CPU 650 @ 3.20GHz x 4 processor, 5.6 GiB of memory, and 983 GB disk. This free machine was just sitting around with nothing to do. What is great about this machine is its ability to always be running and has internet hard wired into it unlike a laptop. This makes it better suited for hosting websites from my home network and it is the perfect fit for the next stage of my dev/testing environment so therefore I commence the beginnings of stage 1.

Moving onward from stage 0 machine capacity has doubled. Now, once changes have been made locally and pushed to Gitlab things are a little different this time. The goal is to have test servers (not production) running for each branch of each repo. Access is granted by `http://<branch>.<repo>.metaheap.io` which could translate to something like `http://www.master.fleetgrid.metaheap.io`. That is for testing on the web, on mobile, under app settings -> “backend url” this can be changed from `http://api.fleetgrid.com` to `http://api.master.fleetgrid.metaheap.io` or perhaps a staging environment like `http://api.powertrade.com` or a clone of this dev/testing environment `http://api.<branch>.<repo>.<clone>`.

In order to achieve my requirements for running test servers I immediately started looking at solutions for CI/CD like Strider CD but landed on a different solution. That is Drone.io which I now have running on the Dell Inspiron and is accessible by url at drone.metaheap.io. The CI/CD pipeline will auto deploy the branches/repos to local test server(s) and then run automated tests. Once reviewed, manually tested, and approved drone.io will then trigger the Gitlab CI/CD pipeline to deploy to our staging environment and then onto production. 

More on [Setting Up Drone to Ship Code Fast](/blog/setting-up-drone-to-ship-code-fast) in another blog post.

Speaking of staging, it should be as close to production as possible. The best way to achieve this is to deploy staging just like production but with a different domain name. When the Gitlab staging pipeline is triggered it must first build each of the node.js services into docker containers. For fleetgrid this would include the frontend, backend, and pdf, plus others, which could take up to 10 minutes to build; that is why deploying to test servers in the previous step is great because of the near instant deployment to a testable url after pushing to git. The catch of why not deploy node.js on bare metal like we did for the test servers is because of the production environment requirements of cloud computing (someone else’s hardware).

Because I can shorten my feedback loop from 10-20 minutes to seconds or a few minutes, I’ve chosen to jump the gun and get a 4 node cluster. I figure if the average developer makes X dollars per hour and spends Y minutes per hour waiting for builds then I could justify spending Z amount to save Y and therefore the cost of X. This will allow me to run up to 4 different versions of an app(s) and here is what the damage looks like.

```
CASE: $44 (power strip + ethernet cable + case) ($7 + $9 + $28)
NODE: $59.5 (power cable + board + heatsink + storage) ($7 + $45 + $0.50 + $7)
TOTAL: $282 (CASE + 4(NODE)) ($44 + 4($59.5))
```

Here is an outline for each of the items:
- case: [GeauxRobot Raspberry Pi 3 Model B 4-layer Dog Bone Stack Clear Case Box Enclosure also for Pi 2B B+ A+ B A](https://www.amazon.com/GeauxRobot-Raspberry-Model-4-layer-Enclosure/dp/B00MYFAAPO/ref=pd_sim_147_3?_encoding=UTF8&pd_rd_i=B00MYFAAPO&pd_rd_r=295932c1-de0e-11e8-b496-e15f3faa0e0e&pd_rd_w=03cBm&pd_rd_wg=XtEbe&pf_rd_i=desktop-dp-sims&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=18bb0b78-4200-49b9-ac91-f141d61a1780&pf_rd_r=FK1J0J7PH22P78R8K0FT&pf_rd_s=desktop-dp-sims&pf_rd_t=40701&psc=1&refRID=FK1J0J7PH22P78R8K0FT)
- ethernet cable:
[Cat 6 Ethernet Cable 3 ft Black (6 Pack)](https://www.amazon.com/Cat-Ethernet-Cable-Black-Pack/dp/B01IQWGUZW/ref=pd_sim_147_2?_encoding=UTF8&pd_rd_i=B01IQWGUZW&pd_rd_r=2f885ddb-e2c9-11e8-bb4e-ddb7811c53a7&pd_rd_w=AzmHB&pd_rd_wg=x0YH0&pf_rd_i=desktop-dp-sims&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=18bb0b78-4200-49b9-ac91-f141d61a1780&pf_rd_r=S4FYZPW8KS1721RQNA8C&pf_rd_s=desktop-dp-sims&pf_rd_t=40701&psc=1&refRID=S4FYZPW8KS1721RQNA8C)
- power strip: 
[Tripp Lite 6 Outlet Surge Protector Power Strip, 2ft Cord, & $2,500 INSURANCE (TLP602)](https://www.amazon.com/Tripp-Lite-Protector-INSURANCE-TLP602/dp/B00006B82A?th=1)
- board: [ROCK64 SINGLE BOARD COMPUTER](https://www.pine64.org/?product=rock64-media-board-computer)
- heatsink: [ROCK64 / PINE A64 / PINE H64 Heatsink](https://www.pine64.org/?product=rock64pine-a64-heatsink)
- storage: [Samsung 32GB 95MB/s (U1) MicroSD EVO Select Memory Card with Adapter (MB-ME32GA/AM)](https://www.amazon.com/Samsung-MicroSD-Adapter-MB-ME32GA-AM/dp/B06XWN9Q99/ref=pd_bxgy_147_img_3?_encoding=UTF8&pd_rd_i=B06XWN9Q99&pd_rd_r=aa148c79-dcb3-11e8-a8c6-ddaa161c605b&pd_rd_w=EqWGC&pd_rd_wg=78pY0&pf_rd_i=desktop-dp-sims&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=6725dbd6-9917-451d-beba-16af7874e407&pf_rd_r=066Z4QMEJCQS1YSFSQTN&pf_rd_s=desktop-dp-sims&pf_rd_t=40701&psc=1&refRID=066Z4QMEJCQS1YSFSQTN)
- power cable: [ROCK64 / PINEBOOK / PINE H64 5V 3A SWITCHING POWER SUPPLY](https://www.pine64.org/?product=rock64-pinebook-5v-3a-switching-power-supply)

I will also need this 1 time purchase for a [SD/MicroSD card reader](https://www.amazon.com/dp/B009D79VH4/ref=psdc_516872_t3_B07F6T9KHW).

```
SD/MICRO SD CARD READER: $8
```

I could put kubernetes on these nodes (allowing 4+ apps) but this is dev/test and I don’t want to wait for build times like I mentioned earlier. Below I have outlined my plans for all of the websites that I want to host:

- www.metaheap.io -> master branch
- trabur.metaheap.io -> master branch
- fleetgrid -> master branch
    * www.master.fleetgrid.metaheap.io
    * api.master.fleetgrid.metaheap.io
    * etc…
- fleetgrid -> feature1 branch
    * www.feature1.fleetgrid.metaheap.io
    * api.feature1.fleetgrid.metaheap.io
    * etc…
- etc...

If I wanted to scale this out to have more nodes I’d just buy another case and 4 nodes; the catch is the network switch. My current router from google fiber has 4 LAN ports on the back which is a problem because I need 1 port for the load balancer plus 4 ports for the nodes. This means I need to find a switch that supports more ports as 4 nodes + 1 LB = 5 ports. Thankfully I have a 1 year old TP-Link router and it has 1 WAN and 4 LANs which is just perfect for turning it into a switch and plugging it into the back of the google router for 4 additional ports making a total of 7 available ports.

Here is what the config for the load balancer looks like:
```js
// ================
// start: config
// ================
let defaultBackend = '127.0.0.1:8374' // www.metaheap.io
let plex = '127.0.0.1:32400'
let drone = ''
let pool = ['127.0.0.1:8374', '127.0.0.1:7548', '192.168.1.102', '192.168.1.103']

// let pool = ['192.168.1.100', '192.168.1.101', '192.168.1.102', '192.168.1.103']

let repos = [
  {
    id: 'www',
    location: 'https://gitlab.com/metaheap/www.metaheap.io',
    branches: [
      {
        id: 'master',
        server: pool[0]
      }
    ]
  },
  {
    id: 'trabur',
    location: 'https://gitlab.com/metaheap/trabur.metaheap.io',
    branches: [
      {
        id: 'master',
        server: pool[1]
      }
    ]
  },
  {
    id: 'fleetgrid',
    location: 'https://gitlab.com/fleetgrid/fleetgrid',
    branches: [
      {
        id: 'master',
        server: pool[2]
      },
      {
        id: 'feature1',
        server: pool[3]
      }
    ]
  }
]
// ================
// end: config
// ================
```

I said I’d dig into some code and here is the first bit. The idea is to have this configuration where I can go in and easily change something like a branch, location, or subdomain path.

After config I have code to check the URL's domain (`req.headers.host`) in order to proxy requests to to proper repositories/branches/microservices.

Below is the logic I have setup to handle each use case:
```js
var server = http.createServer(function (req, res) {
  var host = req.headers.host.split('.')
  console.log(`host: ${host}`)

  if (host[0] === "127") {
  // *.<branch>.<repo>.metaheap.io
  } else if (host.length === 5) {
  // <branch>.<repo>.metaheap.io
  } else if (host.length === 4) {
  // *.metaheap.io
  } else if (host.length === 3) {
    if (host[1] === "metaheap" && host[2] === "io") {
      // plex.metaheap.io
      if (host[0] === "plex") {
      // drone.metaheap.io
      } else if (host[0] === "drone") {
      // <repo>.metaheap.io
      } else if (repo) {
      // otherwise it is the default backend
      // *.metaheap.io
      } else {
      }
    } else {
    }
  } else if (host.length === 2) {
    // http://metaheap.io -> http://www.metaheap.io
  }
});

console.log(`server: 127.0.0.1:${port}`)
server.listen(port)
```

When a use case is met, such as `http://*.metaheap.io`, we'll add the code inside each of the if else block statements to proxy the user to the targeted locations.

For example:
```js
// otherwise it is the default backend
// *.metaheap.io
} else {
  console.log(`proxy: ${req.headers.host} -> *.metaheap.io`)
  proxy.web(req, res, {
    target: `http://${defaultBackend}` // default backend
  })
}
```

Here is the block of code for proxying the user for `http:<repo>.metaheap.io`:
```js
// <repo>.metaheap.io
} else if (repo) {
  // proxy subdomain to the repo's master branch by default
  function findMaster (branches) {
    return branches.id === 'master'
  }

  // master branch could be setup for this repo in the config...
  let master = repo.branches.find(findMaster)
  if (master) {
    console.log(`proxy: ${req.headers.host} -> master.<repo>.metaheap.io`)
    proxy.web(req, res, {
      target: `http://${master.id}.${repo.id}.${host[1]}.${host[2]}` // master/repo
    })
  } else {
    console.log(`${req.headers.host} has not been implemented.`)
  }
}
```

In the above code snippet we are proxying web requests to the repo's master branch by default. We do this by checking to see if there is a master branch setup in the config for the given repo we are in. If there is then proxy the request to master otherwise log "has not been implemented" to the console.

That completes stage 1 of a JS developer’s path to a successful dev/test environment. Reviewing back to what stage 0 looked liked compared to stage 1 we started out with the bare minimum of just a laptop running dev/test locally and then pushing to the cloud for staging/production. Jumping into stage 1 we kept the same functionality in stage 0 but extended its capabilities such as adding a Dell and a 4 node ARM cluster for a better testing and developer operations experience. What is important to remember about coding is small incremental changes and improvements. This methodology is great for building software which is why I’ve spec’d this out in stages. Start out with what is working and move forward from there with a minimal viable product (MVP). I have been putting some more thought into what I want stage 2 and 3 to look like and it would be really nice to accomplish a few things.

As stage 1 stands the Dell i5 is handling the load balancer, Plex, and done.io which is a messy situation. The most logical next step here is to breakout the load balancer into its own dedicated machine. This will free up space and compute resources for the other tasks that are running on the machine such as the CI/CD pipelines!

I am also finding myself interested in routing and switching and really wanted to get a layer 2+ switch with enough ports to run my current setup and perhaps future growth. Before I jump into getting a switch let me talk about what I am actually interested in. Right now configuration is manual so when a branch or repo changes I have to manually go in and update the code to the proper configuration such as pointing the load balancer to the correct IP/Port. Kubernetes solves this problem with its own service discovery and health checking tools plus some other nice things but k8s is for containers which makes it not available in my situation. Finding a tool to handle this automation and my journey into the microservices world will really help me out in the long run. A good search term like, “node js microservices service discovery” would lean one towards a few options. 

My initial reaction was to write the code to implement this and I immediately looked into node.js core and surrounding libraries to achieve this and I have no doubt, for instance, building a system on top of gunjs (a node.js database) to keep a record of running machines and their IP addresses, could be used to achieve service discovery in 100% JavaScript; which I think would be a great exercise for another day. With code however not only do I want to make small incremental changes but I also don’t want to reinvent the wheel.

So my search on the internet commenced. Who is doing service discovery the best anyways? How does it integrate with node.js? Are there hardware implementations (hmmm what is ADN?) or already established protocols? I immediately found Consul by hashicorp. They have a node.js client library with the tools that I am looking for and a web UI! I ran the old “consul vs *” to try and find other solutions that might compare to this and I found a few, looked up their documentation, and checked how they integrated with node.js then I made a decision. I’ll use consul as my service discovery solution. I did look into the mechanics a bit and found out that etcd, the store that kubernetes uses, and Consul both use the [Raft consensus algorithym](https://raft.github.io/) under the hood. 
