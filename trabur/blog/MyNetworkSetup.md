---
slug: my-network-setup
title: My Network Setup
description: From the fiber jack, to the network box, and then to each computer; Let's go over my network setup.
cover: /static/blog/MyNetworkSetup/logo.jpg
coverPreview: /static/blog/MyNetworkSetup/logo.jpg
tags: ['README', 'CLI', 'git', 'json']
createdAt: 2018-11-13T20:16:25.873Z
---
My Network Setup
========

Part of [My Lab Setup](/blog/my-lab-setup-for-full-stack-javascript-development) is a machine running Ubuntu.