README.md
========
#### Build content:
Convert markdown files to json files that are readable by machines.
```bash
# builds trabur/blog/* and trabur/profolio/*
$ npm run content
```

#### Deploy from CLI:
Make a change to content then push to the cloud.
```bash
# starting from the content folder
$ cd ~/Projects/metaheap/content

# generate .s3cfg by running this command; a backup of config inputs can be found in the gitlab-ci settings under variables for the given repo
$ s3cmd --configure

# the above command will output a .s3cfg file into your home directory ~/

# put files in a bucket
# s3cmd put * s3://<bucket> --acl-public --recursive
$ s3cmd put * s3://metaheap --acl-public --recursive --exclude="node_modules/*"

# delete all files in a bucket
# s3cmd rm s3://<bucket>  --recursive --force
$ s3cmd rm s3://metaheap  --recursive --force
```

#### Deploy from CI/CD:
Here is a `gitlab-ci.yml` file that will auto deploy content after a commit to git.
```yaml
stages:
  - upload

upload:
  stage: upload
  image: debian:stretch
  script:
    - apt-get update
    - apt-get install -y s3cmd
    - s3cmd put * s3://$BUCKET --acl-public --recursive --config=.s3cfg
  only:
    - master
```